

int motorLeft1 = 2;       // Левый мотор.
int motorLeft2 = 4;
int enableLeft = 3;

int motorRight1 = 6;        // Правый мотор.
int motorRight2 = 7;
int enableRight = 5;

int control;
int motorSpeed = 190;
void setup(){
  pinMode (motorRight1, OUTPUT);
  pinMode (motorRight2, OUTPUT);
  pinMode (enableRight, OUTPUT);

  pinMode (motorLeft1, OUTPUT);
  pinMode (motorLeft2, OUTPUT);
  pinMode (enableLeft, OUTPUT);

  Serial.begin (9600);
  

}

void loop(){

  if (Serial.available() > 0 ){
    control = Serial.read();
    if (control == '1'){
      leftForward();
      rightForward();
      delay(2000);
      rightStop();
      leftStop();
    }
    if (control == '2'){
      leftBack();
      rightBack();
      delay(2000);
      rightStop();
      leftStop();
    }
    if (control == '3') {
      turnForwardRight();
      delay(3000);
      rightStop();
      leftStop();
    }
    if(control == '4'){
      turnForwardLeft();
      delay(2000);
      rightStop();
      leftStop();
    }
    if(control == '6'){
      turnBackLeft();
      delay(2000);
      rightStop();
      leftStop();
    }
    if(control == '7'){
      turnBackRight();
      delay(2000);
      rightStop();
      leftStop();
    }
  
  }
}
// Движение вперед
void rightForward(){
  digitalWrite(motorRight1, HIGH);
  digitalWrite(motorRight2, LOW);
  analogWrite(enableRight, motorSpeed);
}

void leftForward(){
  digitalWrite(motorLeft1, LOW);
  digitalWrite(motorLeft2, HIGH);
  analogWrite(enableLeft, motorSpeed);
}

// Движение назад
void rightBack(){
  digitalWrite(motorRight1, LOW);
  digitalWrite(motorRight2, HIGH);
  analogWrite(enableRight, motorSpeed);
}
void leftBack(){
  digitalWrite(motorLeft1, HIGH);
  digitalWrite(motorLeft2, LOW);
  analogWrite(enableLeft, motorSpeed);
}

// Остановка

void rightStop(){
  digitalWrite(motorRight1, LOW);
  digitalWrite(motorRight2, LOW);
  analogWrite(enableRight, 0);
}
void leftStop(){
  digitalWrite(motorLeft1, LOW);
  digitalWrite(motorLeft2, LOW);
  analogWrite(enableLeft, 0);
}


// Поворот вперед направо

void turnForwardRight(){
  rightStop();
  leftForward();
}

// Поворот вперед налево

void turnForwardLeft(){
  leftStop();
  rightForward();
}

// Поворот назад налево

void turnBackLeft(){
  rightStop();
  leftBack();
}

void turnBackRight(){
  leftStop();
  rightBack();
}
