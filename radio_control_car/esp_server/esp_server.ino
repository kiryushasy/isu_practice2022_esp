#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>

#include <SoftwareSerial.h>

ESP8266WebServer server(80);

String ssidAP = "ESP_WIFI";
String passwordAP = "ESP8266123";
void setup(){
  Serial.begin(9600);
  WiFi.softAP(ssidAP, passwordAP);
  IPAddress myIP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(myIP);
  server.on("/", handleRoot);
  server.on("/forward", forward);
  server.on("/back", back);
  server.on("/turn", turn);
  server.on("/left", left);
  server.on("/turnBackL", turnBackL);
  server.on("/turnBackR", turnBackR);
  server.begin();
  Serial.println("HTTP server started");
}

String WebPage(){
  String web = "<!DOCTYPE html <html>\n>";
  web += "<head><meta name = \"viewport\" content = \"width = device-width, initial-scale=1.0, user-scalable=no\">\n";
  web += "<title>WiFi RC for car </title>\n";
  web += "<style> .forward {display: block;width: 80px;background-color: #1abc9c;border: none;color: white;padding: 13px 30px;text-decoration: none;font-size: 25px;margin: 0px auto 35px;cursor: pointer;border-radius: 4px;}\n";
  web += ".back {display: block;width: 80px;background-color: #1abc9c;border: none;color: white;padding: 13px 30px;text-decoration: none;font-size: 25px;margin: 0px auto 35px;cursor: pointer;border-radius: 4px;}\n";
  web += ".turnR {display: block;width: 80px;background-color: #1abc9c;border: none;color: white;padding: 13px 30px;text-decoration: none;font-size: 25px;margin: 0px auto 35px;cursor: pointer;border-radius: 4px;}\n";
  web += ".turnL {display: block;width: 80px;background-color: #1abc9c;border: none;color: white;padding: 13px 30px;text-decoration: none;font-size: 25px;margin: 0px auto 35px;cursor: pointer;border-radius: 4px;}\n";
  web += ".turnBackL {display: block;width: 80px;background-color: #1abc9c;border: none;color: white;padding: 13px 30px;text-decoration: none;font-size: 25px;margin: 0px auto 35px;cursor: pointer;border-radius: 4px;}\n";
  web += ".turnBackR {display: block;width: 80px;background-color: #1abc9c;border: none;color: white;padding: 13px 30px;text-decoration: none;font-size: 25px;margin: 0px auto 35px;cursor: pointer;border-radius: 4px;}\n";
  web += "</style>\n";
  web += "</head>\n";
  web += "<body>\n";
  web += "<div> <a  class = \"forward\" href = \"/forward\" >Forward</a> </div>\n";
  web += "<div> <a class = \"turnR\" href = \"/turn\" >Turn Right</a> </div>\n";
  web+= "<div> <a class = \"turnL\" href = \"/left\">Turn Left</a>\n";
  web += "<div> <a class = \"turnBackL\"  href = \"/turnBackL\" >Turn Back left</a> </div>\n";
  web += "<div> <a class = \"turnBackR\"  href = \"/turnBackR\" >Turn Back right</a> </div>\n";
  web += "<div> <a class = \"back\"  href = \"/back\" >Back</a> </div>\n";
  

  web += "</body>\n";

  web += "</html>\n";
  return web;
  
}

void handleRoot(){
   server.send(200, "text/html", WebPage());
}

void forward(){
  Serial.write("1");
  delay(2000);
  server.send(200, "text/html", WebPage());
  
}
void back(){
  Serial.write("2");
  delay(2000);
  server.send(200, "text/html", WebPage());
}
void turn(){
  Serial.write("3");
  delay(2000);
  server.send(200, "text/html", WebPage());
}
void left(){
  Serial.write("4");
  delay(2000);
  server.send(200,"text/html", WebPage());
}
void turnBackL(){
  Serial.write("6");
  delay(2000);
  server.send(200, "text/html", WebPage());
}
void turnBackR(){
  Serial.write("7");
  delay(2000);
  server.send(200, "text/html", WebPage());
}
void loop(){
  server.handleClient();
  
}
